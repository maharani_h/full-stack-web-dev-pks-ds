<?php

namespace App\Listeners;

use App\Mail\RegisterOTP;
use App\Mail\RegenerateOTP;
use Illuminate\Support\Facades\Mail;
use App\Events\OtpStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToGiveOTP implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to($event->$otp->newusers->email)->send(new RegistereOTP($event->$otp));
        Mail::to($event->$otp->newusers->email)->send(new RegenerateOTP($event->$otp));

    }
}
