<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name'];

    public function newusers()
    {
        return $this->thisMany('App\newusers');
    }
}
