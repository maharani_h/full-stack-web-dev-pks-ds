<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class newusers extends Model
{
    protected $table = 'newusers';
    protected $fillable = ['username', 'email', 'name', 'roles_id', 'password', 'email_verified_at'];

    public function roles()
    {
        return $this->belongsTo('App\roles');
    }

    public function OtpCode()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
