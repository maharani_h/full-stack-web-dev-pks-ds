<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleContorller extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table roles
        $roles = roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Role',
            'data'    => $roles  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find roles by ID
        $roles = roles::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data'    => $roles 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = roles::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $roles  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $roles
     * @return void
     */
    public function update(Request $request, roles $roles)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find roles by ID
        $roles = roles::findOrFail($roles->id);

        if($roles) {

            //update roles
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $roles  
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $roles = roles::findOrfail($id);

        if($roles) {

            //delete roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
