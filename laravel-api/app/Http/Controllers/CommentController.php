<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
     /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table comments
        $comments = Comments::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comments',
            'data'    => $comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comments by ID
        $comments = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comments',
            'data'    => $comments 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'posts_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comment::create([
            'content'     => $request->content,
            'posts_id'   => $request->posts_id
        ]);

        //Memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comments));


        // Mail::to($comments->post->newusers->email)->send(new PostAuthorMail($comments));
        // Mail::to($comments->newusers->newusers->email)->send(new CommentAuthorMail($comments));


        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Comments Created',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comments Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comments
     * @return void
     */
    public function update(Request $request, Comments $comments)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'posts_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comments = Comments::findOrFail($comments->id);

        if($comments) 
        {
            $user = auth()->user();
            
            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Komentar ini bukan milik Anda',
                ], 403);  
            }

            //update post
            $comments->update([
                'content'     => $request->content,
                'posts_id'   => $request->posts_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comments  
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Comments Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comments by ID
        $comments = Comments::findOrfail($id);

        if($comments) 
        {
            $user = auth()->user();
            
            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Komentar ini bukan milik Anda',
                ], 403);  
            }

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comments Deleted',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Comments Not Found',
        ], 404);
    }
}
