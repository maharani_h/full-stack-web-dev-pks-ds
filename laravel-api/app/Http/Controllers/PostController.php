<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{ 
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $posts = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $posts  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $posts = Post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $posts
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        

        //save to database
        $posts = Post::create([
            'title'     => $request->title,
            'description'   => $request->description, 
        ]);

        //success save to database
        if($posts) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $posts
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $posts
     * @return void
     */
    public function update(Request $request, Post $posts)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $posts = Post::findOrFail($post->id);

        

        if($posts) {
            
            $user = auth()->user();
            
            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Post ini bukan milik Anda',
                ], 403);  
            }
            
            //update post
            $posts->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $posts  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $posts = Post::findOrfail($id);

        if($posts) 
        {
            $user = auth()->user();
            
            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Post ini bukan milik Anda',
                ], 403);  
            }
            //delete post
            $posts->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
