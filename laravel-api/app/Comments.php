<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';
    protected $fillable = ['content', 'posts_id', 'user_id'];

    public function Post()
    {
        return $this->belongsTo('App\Post');
    }

    public function newusers()
    {
        return $this->belongsTo('App\newusers');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->user_id = auth()->user()->id;
        });
    }
}
