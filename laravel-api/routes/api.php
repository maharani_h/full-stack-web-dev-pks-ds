<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource post
 */
Route::middleware('auth.api')->get('/newusers', function(Request $request){
    return $request->newusers();
});

Route::apiResource('/post', 'PostController');

Route::apiResource('/roles', 'RoleContorller');

Route::apiResource('/comments', 'CommentController');

Route::post('/auth/register', 'Auth\RegisterController');
