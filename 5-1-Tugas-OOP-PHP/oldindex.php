<?php

//Kelas Utama (Parent Class): Hewan dan Fight
class hewan{
    //property
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    //constructor dan destructor
    // public function __construct(){

    // }
    
    //metode set && metode get

    public function set_nama($nama){
        $this->nama = $nama;
    }
    public function get_nama(){
        return $this->nama;
    }

    public function set_darah($darah){
        $this->darah = $darah;
    }
    public function get_darah(){
        return $this->darah;
    }

    public function set_jumlahkaki($jumlahkaki){
        $this->jumlahkaki = $jumlahkaki;
    }
    public function get_jumlahkaki(){
        return $this->jumlahkaki;
    }

    public function set_keahlian($keahlian){
        $this->keahlian = $keahlian;
    }
    public function get_keahlian(){
        return $this->keahlian;
    }

   public function atraksi(){
       echo $this->nama . ' sedang ' . $this->keahlian;
   }
}


class fight extends hewan{
    //property
    public $attackpower;
    public $defencepower;


    public function set_attackpower($attackpower){
        $this->attackpower = $attackpower;
    }
    public function get_attackpower(){
        return $this->attackpower;
    }

    public function set_defencepower($defencepower){
        $this->defencepower = $defencepower;
    }
    public function get_defencepower(){
        return $this->defencepower;
    }

    //metode
    
    public function serang(){
       echo $this->nama . 'sedang menyerang' . $this->nama;
    }

    public function diserang(){
        echo $this->nama . 'sedang diserang' . $this->nama;
    }

}





//Kelas Turunan (Child Class): Harimau dan Elang
class harimau extends fight{
    //metode:
   public function getinfohewan(){
      echo 'Nama Hewan: ' .$this->nama . '<br>' .'Banyak Darah: ' . $this->darah . '<br>' . 'Jumlah Kaki: ' . $this->jumlahkaki . '<br>' . 'Keahlian: ' . $this->keahlian;
   }

}


class elang extends fight{
    public function getinfohewan(){
        echo 'Nama Hewan: ' .$this->nama . '<br>' .'Banyak Darah: ' . $this->darah . '<br>' . 'Jumlah Kaki: ' . $this->jumlahkaki . '<br>' . 'Keahlian: ' . $this->keahlian;
     }
}


$harimau = new harimau;
$harimau->set_nama('Harimau');
$harimau->set_jumlahkaki(4);
$harimau->set_keahlian('lari cepat');
$harimau->set_attackpower(7);
$harimau->set_defencepower(8);


$elang = new elang;
$elang->set_nama('Elang');
$elang->set_jumlahkaki(2);
$elang->set_keahlian('terbang tinggi');
$elang->set_attackpower(10);
$elang->set_defencepower(5);


echo $harimau->getinfohewan();
echo "<br>";
echo "<br>";
echo $elang->getinfohewan();
echo "<br>";
echo "<br>";

echo $harimau->atraksi();
echo "<br>";
echo $elang->atraksi();

echo "<br>";
echo "<br>";
echo $harimau  ->serang();
?>