<?php

trait hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi()
    {
        echo "$this->nama sedang $this->keahlian";
    }
}


abstract class fight
{
    use hewan;

    public $attackpower;
    public $defencepower;

    public function serang($hewan)
    {
        echo "$this->nama sedang menyerang $hewan->nama";
        echo "<br>";
        $hewan->diserang($this);
    }
 
    public function diserang($hewan)
    {
        echo "$this->nama sedang diserang $hewan->nama";

        $this->darah = $this->darah - ($hewan->attackpower / $this->defencepower);
    }

    protected function getinfo()
    {
        echo "Nama: ($this->nama)";
        echo "<br>";
        echo "Jumlah Kaki: ($this->jumlahkaki)";
        echo "<br>";
        echo "Keahlian: ($this->keahlian)";
        echo "<br>";
        echo "Darah: ($this->darah)";
        echo "<br>";
        echo "Kekuatan Menyerang: ($this->attackpower)";
        echo "<br>";
        echo "Kekuatan Pertahanan: ($this->defencepower)";
    }

    abstract public function getinfohewan();

}



class harimau extends fight
{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->defencepower = 8;
    }

    public function getinfohewan()
    {
        echo "Jenis Hewan: Harimau";
        echo "<br>";
        $this->getinfo();
    }
}


class elang extends fight
{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackpower = 10;
        $this->defencepower = 5;
    }

    public function getinfohewan()
    {
        echo "Jenis Hewan: Elang";
        echo "<br>";
        $this->getinfo();
    } 
}


class enter
{
    public static function buatenter()
    {
        echo "<br>";
        echo "_______________________";
        echo "<br>";
    }
}



$harimau = new harimau("Harimau Bengali");
$harimau->getinfohewan();
enter::buatenter();
$elang = new elang("Elang Botak");
$elang->getinfohewan();
enter::buatenter();
$harimau->atraksi();
enter::buatenter();
$elang->atraksi();
enter::buatenter();
$harimau->serang($elang);
enter::buatenter();
$elang->getinfohewan();
enter::buatenter();
$elang->serang($harimau);
enter::buatenter();
$harimau->getinfohewan();
enter::buatenter();
?>